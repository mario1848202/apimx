var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var movimientosv2JSON = require('./movimientosv2.json');
//var bodyparser = require('body-parser');
//app.use(bodyparser.json());

app.listen(port);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function (req,res,next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/mmartinezr/collections/";
var apiKeyMlab = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz;
var urlClientes = "https://api.mlab.com/api/1/databases/mmartinezr/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab = requestjson.createClient(urlClientes);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function (req,res) {
  //res.send('Hola mundo desde nodeJS');
  res.sendFile(path.join(__dirname,'index.html'));
});

app.post('/',function (req,res) {
  res.send('Se ha recibido la petición post');
});

app.put('/',function (req,res) {
  res.send('Se ha recibido la petición put');
});

app.delete('/',function (req,res) {
  res.send('Se ha recibido la petición delete cambiada');
});

app.get('/Clientes/:idcliente',function (req,res) {
  res.send('idCliente: '+req.params.idcliente);
});

app.get('/v1/Movimientos',function (req,res) {
  res.sendfile('movimientosv1.json');
});

app.get('/v2/Movimientos',function (req,res) {
  res.json(movimientosv2JSON);
});

app.get('/v2/Movimientos/:id',function (req,res) {
  console.log(req.params.id);
  res.send(movimientosv2JSON[req.params.id]);
});

app.get('/v2/Movimientosquery',function (req,res) {
  console.log(req.query);
  res.send('Recibido ' + req.query.nombre);
});

app.post('/v2/Movimientos',function (req,res) {
  var nuevo = req.body;
  nuevo.id = movimientosv2JSON.length + 1;
  movimientosv2JSON.push(nuevo);
  res.send('Movimiento dado de alta con id: '+ nuevo.id);
});

app.get('/Clientes',function (req,res) {
  clienteMlab.get('',function (err, resM, body) {
    if (err) {
      console.log(body);
    }else{
      res.send(body);
    }
  })
});

app.post('/Clientes',function (req,res) {
  clienteMlab.post('', req.body, function (err, resM, body) {
    res.send(body);
  })
});

app.post('/login', function (req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+email+'","password":"'+password+'"}';


  console.log(query);

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "/Usuarios?" + apiKeyMlab + "&" + query);
  console.log(urlMlabRaiz + "/Usuarios?" + apiKeyMlab + "&" + query);

  clienteMlabRaiz.get('',function (err, resM, body) {
    if (!err) {
      if (body.length == 1) {//Login ok
        res.status(200).send('Usuario Logado');
      }else {//Login not ok
        res.status(400).send('Usuario no encontrado');
      }
    }
  });
});
