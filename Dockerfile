#Imagen base
FROM node:latest

#Directorio de la app en el contenedor
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto que se expone
EXPOSE 3000

#Comandos de ejecucion de la aplicacion
CMD ["npm", "start"]
